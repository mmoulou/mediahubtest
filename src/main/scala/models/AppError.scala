package models

sealed trait AppError extends Throwable

case class DecodingError(message: String, row: String) extends AppError
