package flow

import java.util.UUID

import akka.actor.ActorSystem
import akka.stream.scaladsl.{Sink, Source}
import akka.stream.{ActorMaterializer, ActorMaterializerSettings, IOResult}
import models.Title
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{FlatSpec, Matchers}

import scala.concurrent.Future

class TitleFlowTest extends FlatSpec with Matchers with ScalaFutures {

  implicit val system: ActorSystem = ActorSystem("TitleFlowTest")
  implicit val materializer: ActorMaterializer = ActorMaterializer(ActorMaterializerSettings(system))

  val title1 = Title.Id(UUID.randomUUID().toString)
  val title2 = Title.Id(UUID.randomUUID().toString)
  val title3 = Title.Id(UUID.randomUUID().toString)
  val title4 = Title.Id(UUID.randomUUID().toString)
  val titles = List(
    Title(title1, "Movie", "title 1", "title 1", false, None, None, None, List.empty),
    Title(title2, "Movie", "title 2", "title 2", false, None, None, None, List.empty),
    Title(title3, "Movie", "title 3", "title 3", false, None, None, None, List.empty),
    Title(title4, "Movie", "title 2", "title 4", false, None, None, None, List.empty)
  )

  def source = Source(titles).mapMaterializedValue(_ => Future.successful(IOResult.createSuccessful(1)))
  def titleFlow = new TitleFlow(source)

  "title flow" should "return None  if searched title is not found" in {

    val result = titleFlow.getTitle("toto").runWith(Sink.headOption)
    result.futureValue shouldBe None
  }

  "title flow" should "return first title found, even if there is many title with the same primaryTitle" in {

    val result = titleFlow.getTitle("title 2").runWith(Sink.headOption).futureValue
    result.map(_.tconst) shouldBe Some(title2)
  }

  "title flow" should "return empty titles if not found" in {
    val result = titleFlow.getTitles(Seq(Title.Id("toto"))).runWith(Sink.seq).futureValue

    result.isEmpty shouldBe true
  }

  "title flow" should "return list of founded titles" in {
    val result = titleFlow.getTitles(Seq(Title.Id("toto"), title1)).runWith(Sink.seq).futureValue

    result.size shouldBe 1
    result.head.tconst shouldBe title1
  }
}
