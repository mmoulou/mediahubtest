package flow

import java.util.UUID

import akka.actor.ActorSystem
import akka.stream.scaladsl.{Sink, Source}
import akka.stream.{ActorMaterializer, ActorMaterializerSettings, IOResult}
import models.{Episode, Title}
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{FlatSpec, Matchers}

import scala.concurrent.Future

class EpisodeFlowTest extends  FlatSpec with Matchers with ScalaFutures {

  implicit val system: ActorSystem = ActorSystem("EpisodeFlowTest")
  implicit val materializer: ActorMaterializer = ActorMaterializer(ActorMaterializerSettings(system))

  val serie1 = Title.Id(UUID.randomUUID().toString)
  val serie2 = Title.Id(UUID.randomUUID().toString)
  val serie3 = Title.Id(UUID.randomUUID().toString)

  def episodeId = Episode.Id(UUID.randomUUID().toString)


  "episode flow" should " get TvSeries with greatest Number of episodes" in {

    val episodes = List(
      Episode(episodeId, serie1, None, None),
      Episode(episodeId, serie1, None, None),
      Episode(episodeId, serie1, None, None),
      Episode(episodeId, serie1, None, None),
      Episode(episodeId, serie1, None, None),
      Episode(episodeId, serie2, None, None),
      Episode(episodeId, serie2, None, None),
      Episode(episodeId, serie3, None, None),
      Episode(episodeId, serie3, None, None),
      Episode(episodeId, serie3, None, None),
    )

    val source: Source[Episode, Future[IOResult]] = Source(episodes).mapMaterializedValue(_ => Future.successful(IOResult.createSuccessful(1)))

    val flow = new EpisodeFlow(source)

    val result = flow.getTvSeriesIdWithGreatestNumberOfEpisode(2).runWith(Sink.head)

    val seriesIdWithCountEpisode = result.futureValue.toMap

    seriesIdWithCountEpisode(serie1) shouldBe 5
    seriesIdWithCountEpisode(serie3) shouldBe 3
    seriesIdWithCountEpisode.get(serie2) shouldBe None
  }




}
