package object models {

  private val TsvNoneValue = "\\N"
  def orNone(value: String): Option[String] =
    if(value.equals(TsvNoneValue)) None else Some(value)
}
