package models

final case class Rating(
                         tconst: Title.Id,
                         averageRating: Int,
                         numVotes: Int)

object Rating{
  implicit val transformTsvLine: List[String] => Rating = { row: List[String] =>
    Rating(Title.Id(row.head), row(1).toInt, row(2).toInt)
  }
}