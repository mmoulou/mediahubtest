package models

final case class Name(
                       nconst: Name.Id,
                       primaryName: String,
                       birthYear: Option[Int],
                       deathYear: Option[Int],
                       primaryProfession: String,
                       knownForTitles: String)

object Name {
  case class Id(value: String)

  implicit val transformTsvLine: List[String] => Name = { row: List[String] =>
    Name(
      Name.Id(row.head),
      row(1),
      orNone(row(2)).map(_.toInt),
      orNone(row(3)).map(_.toInt),
      row(4),
      row(5)
    )
  }
}
