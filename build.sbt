import Dependencies._

lazy val mediaHubTest = (project in file(".")).settings(
  inThisBuild(List(

    name := "mediaHubTest",
    organization := "com.mediahub",
    scalaVersion := "2.12.9",
    scalacOptions ++= Seq(
      "-Xfatal-warnings",
      "-feature",
      "-Yrangepos",
      "-Xlint",
      "-deprecation",
      "-feature",
      "-encoding", "UTF-8",
      "-unchecked",
      "-Yno-adapted-args",
      "-Ypartial-unification",
      "-Ywarn-dead-code",
      "-Ywarn-numeric-widen",
      "-Ywarn-value-discard",
      "-Xfuture",
      "-Ywarn-unused",
      "-Ydelambdafy:method",
      "-target:jvm-1.8"
    ),
    libraryDependencies ++= Seq(
      akkaStreams,
      akkaSlf4j,
      logback,
      akkaStreamsCsv,
      akkaTestKit % Test,
      scalaTest % Test
    ),
    fork in Test := true,
    fork in run := true
  ))
)
