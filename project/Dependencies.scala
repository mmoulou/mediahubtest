import sbt._

object Dependencies {

  val akkaVersion = "2.5.6"
  val alpakkaVersion = "1.1.2"

  val akkaStreams = "com.typesafe.akka" %% "akka-stream" % akkaVersion
  val akkaStreamsCsv = "com.lightbend.akka" %% "akka-stream-alpakka-csv" % alpakkaVersion
  val akkaSlf4j = "com.typesafe.akka" %% "akka-slf4j" % akkaVersion
  val logback = "ch.qos.logback" % "logback-classic" % "1.2.3"

  val akkaTestKit = "com.typesafe.akka" %% "akka-stream-testkit" % akkaVersion
  val scalaTest = "org.scalatest" %% "scalatest" % "3.0.8"
}
