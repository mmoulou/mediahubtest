package models

final case class Episode(
                          tconst: Episode.Id,
                          parentTconst: Title.Id,
                          seasonNumber: Option[Int],
                          episodeNumber: Option[Int])

object Episode {
  case class Id(value: String)
  implicit val transformTsvLine: List[String] => Episode = { row: List[String] =>
    Episode(Id(row.head), Title.Id(row(1)), orNone(row(2)).map(_.toInt), orNone(row(3)).map(_.toInt))
  }
}
