package models

final case class Principal(
                            tconst: Title.Id,
                            ordering: Int,
                            nconst: Name.Id,
                            category: String,
                            job: Option[String],
                            characters: Option[String])

object Principal{
  implicit val transformTsvLine: List[String] => Principal = { row: List[String] =>
    Principal(
      Title.Id(row.head),
      row(1).toInt,
      Name.Id(row(2)),
      row(3),
      orNone(row(4)),
      orNone(row(5)))
  }
}