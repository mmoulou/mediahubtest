package flow

import akka.stream.IOResult
import akka.stream.scaladsl.Source
import models.Name
import org.slf4j.LoggerFactory

import scala.concurrent.Future

class NameFlow(source: Source[Name, Future[IOResult]]) {
  val logger = LoggerFactory.getLogger(getClass)

  def getPersons(nameIds: Seq[Name.Id]): Source[Name, Future[IOResult]] = {
    logger.info(s"get persons with nameIds: $nameIds")
    source.filter(name => nameIds.contains(name.nconst))
  }
}