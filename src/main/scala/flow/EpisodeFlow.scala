package flow

import akka.stream.IOResult
import akka.stream.scaladsl.Source
import models.{Episode, Title}

import scala.collection.mutable
import scala.collection.mutable.PriorityQueue
import scala.concurrent.Future

class EpisodeFlow(source: Source[Episode, Future[IOResult]]) {

  def getTvSeriesIdWithGreatestNumberOfEpisode(n: Long): Source[mutable.PriorityQueue[(Title.Id, Int)], Future[IOResult]] = {

    source
      .map(episode => episode.parentTconst -> 1)
      .groupBy(Int.MaxValue, _._1)
      .reduce((l, r) => (l._1, l._2 + r._2))
      .mergeSubstreams
      .fold(PriorityQueue.empty[(Title.Id, Int)]){ case (queue, element) =>
        queue.enqueue(element)
        if(queue.size > n) queue.dequeue() else ()
        queue
      }
  }

  implicit val ordering: Ordering[(Title.Id, Int)] = Ordering.by(_._2 * -1)
}
