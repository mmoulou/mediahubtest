package flow

import akka.stream.IOResult
import akka.stream.scaladsl.Source
import models.{Principal, Title}
import org.slf4j.LoggerFactory

import scala.concurrent.Future

class PrincipalFlow (source: Source[Principal, Future[IOResult]]) {
  val logger = LoggerFactory.getLogger(getClass)

  def getPrincipalsWithTitleId(titleId: Title.Id): Source[Principal, Future[IOResult]] = {
    logger.info(s"get principals who have titleId: $titleId")
    source.filter(_.tconst.equals(titleId))
  }
}
