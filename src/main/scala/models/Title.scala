package models

import java.util.concurrent.TimeUnit

import scala.concurrent.duration._

case class Title(
                  tconst: Title.Id,
                  titleType: String,
                  primaryTitle: String,
                  originalTitle: String,
                  isAdult: Boolean,
                  startYear: Option[Int],
                  endYear: Option[Int],
                  runtimeMinutes: Option[Duration],
                  genres: List[String])

object Title {
  case class Id(value: String)
  implicit val transformTsvLine: List[String] => Title = { row : List[String] =>
    Title(
      Id(row.head),
      row(1),
      row(2),
      row(3),
      row(4).toInt != 0,
      orNone(row(5)).map(_.toInt),
      orNone(row(6)).map(_.toInt),
      orNone(row(7)).map(v => Duration(v.toLong, TimeUnit.MINUTES)),
      row(8).split(",").toList
    )
  }
}
