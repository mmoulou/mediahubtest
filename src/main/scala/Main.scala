
import java.io.File

import akka.actor.ActorSystem
import akka.stream
import akka.stream.scaladsl._
import akka.stream.{ActorMaterializer, ActorMaterializerSettings}
import flow._
import models._
import org.slf4j.LoggerFactory

import scala.concurrent.Future

object Main  extends App{

  private val logger = LoggerFactory.getLogger(getClass)

  private val TvSeriesChoice = "tvSeries"
  private val PrincipalForMovieChoice = "principals"

  val decider: stream.Supervision.Decider = {
    case DecodingError(message, row) =>
      logger.debug(s"decoding exception, could not decode row: $row, message: $message")
      logger.error(s"decoding exception, message: $message")
      stream.Supervision.Resume
    case _ =>
      logger.error("Uncaught exception, stopping flow")
      stream.Supervision.Stop
  }

  implicit val system: ActorSystem = ActorSystem("mediaHubTest")
  implicit val materializer: ActorMaterializer = ActorMaterializer(ActorMaterializerSettings(system).withSupervisionStrategy(decider))
  import system.dispatcher

  if(args.isEmpty) {
    println("""                Usage:            """)
    println(""" If you want to get Principals for Movie: PRINCIPALS $data_source_folder_path $movie_name """)
    println(""" If you want to get tvSeries with greatest number of episodes:  TVSERIES $data_source_folder_path""")
  } else if(args.length == 2 && args.head.equalsIgnoreCase(TvSeriesChoice)) tvSeriesWithGreatestNumberOfEpisodes(args(1))
  else if(args.length == 3 && args.head.equalsIgnoreCase(PrincipalForMovieChoice)) principalsForMovieName(args(1), args(2))


  private def principalsForMovieName(dataSourceFolderPath: String, name: String)(implicit materializer: ActorMaterializer) = {
    val nameSource = new IOFileSource[Name](s"$dataSourceFolderPath${File.separator}name.basics.tsv").getSource
    val principalSource = new IOFileSource[Principal](s"$dataSourceFolderPath${File.separator}title.principals.tsv").getSource
    val titleSource = new IOFileSource[Title](s"$dataSourceFolderPath${File.separator}title.basics.tsv").getSource

    val nameFlow = new NameFlow(nameSource)
    val principalFlow = new PrincipalFlow(principalSource)
    val titleFlow = new TitleFlow(titleSource)

    val actors: Future[Seq[Name]] = for {
      titleId <- titleFlow.getTitle(name).runWith(Sink.headOption).map(_.map(_.tconst))
      principalIds <-
        titleId.fold(Future.successful(Seq.empty[Name.Id]))(id => principalFlow.getPrincipalsWithTitleId(id).map(_.nconst).runWith(Sink.seq[Name.Id]))
      names <- nameFlow.getPersons(principalIds).runWith(Sink.seq[Name])
    } yield names

    actors.map{ names =>
      println(s"Principals for movie name: $name")
      names.foreach(println)
    }
  }

  private def tvSeriesWithGreatestNumberOfEpisodes(dataSourceFolderPath: String)(implicit materializer: ActorMaterializer) = {
    val episodeSource = new IOFileSource[Episode](s"$dataSourceFolderPath${File.separator}title.episode.tsv").getSource
    val titleSource = new IOFileSource[Title](s"$dataSourceFolderPath${File.separator}title.basics.tsv").getSource
    val episodeFlow = new EpisodeFlow(episodeSource)
    val titleFlow = new TitleFlow(titleSource)

    val tvSeries = for {
      tvSeriesIdWithTotalEpisodes <- episodeFlow.getTvSeriesIdWithGreatestNumberOfEpisode(10).runWith(Sink.head)
      titles <- titleFlow.getTitles(tvSeriesIdWithTotalEpisodes.map(_._1).toSeq).runWith(Sink.seq)
    } yield titles

    tvSeries.map{
      titles =>
        println(s"Tv Series with greatest number of episodes: ")
        titles.foreach(println)
    }
  }
}
