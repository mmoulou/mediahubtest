package flow

import java.nio.charset.StandardCharsets
import java.nio.file.Paths

import akka.NotUsed
import akka.stream.IOResult
import akka.stream.alpakka.csv.scaladsl.CsvParsing
import akka.stream.scaladsl.{FileIO, Flow, Source}
import akka.util.ByteString
import models.DecodingError

import scala.concurrent.Future
import scala.util.Try
import scala.util.control.NonFatal

sealed trait SourceParser[A, Mat] {

  def getSource(implicit transform: List[String] => A): Source[A, Mat]
}

class IOFileSource[A](path: String) extends SourceParser[A, Future[IOResult]] {
  override def getSource(implicit transformLine: List[String] => A): Source[A, Future[IOResult]] = {
    val file = Paths.get(path)

    val flowParser: Flow[ByteString, List[ByteString], NotUsed] = CsvParsing.lineScanner(delimiter = '\u0009', quoteChar = '|', maximumLineLength = CsvParsing.maximumLineLengthDefault * 4)

    val source: Source[ByteString, Future[IOResult]] = FileIO.fromPath(file)

    source
      .via(flowParser)
      .map(_.map(_.decodeString(StandardCharsets.UTF_8)))
      .drop(1)
      .map{
        row =>
          Try(transformLine(row)).fold(
            { case NonFatal(e) => throw DecodingError(e.getMessage, row.mkString("\\t")) },
            identity
          )
      }
  }
}