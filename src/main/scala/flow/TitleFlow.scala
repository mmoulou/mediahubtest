package flow

import akka.stream.IOResult
import akka.stream.scaladsl.Source
import models.Title
import org.slf4j.LoggerFactory

import scala.concurrent.Future

class TitleFlow (source: Source[Title, Future[IOResult]])  {

  val logger = LoggerFactory.getLogger(getClass)

  def getTitle(name: String): Source[Title, Future[IOResult]] = {
    logger.info(s"get title with name: $name")
    source
      .filter(title => title.originalTitle.equalsIgnoreCase(name) || title.primaryTitle.equalsIgnoreCase(name))
  }

  def getTitles(titleIds: Seq[Title.Id]): Source[Title, Future[IOResult]] = {
    logger.info(s"find titles with id: $titleIds")
    source.filter(title => titleIds.contains(title.tconst))
  }
}
